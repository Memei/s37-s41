const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Routes:
// checkEmail routes - checks if email is existing from our database
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// register route - create user in our DB collection.

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get('/details', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getSingleUser({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Routes for enrolling a user
router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		res.send("Admin access detected. Unable to process your request.")
	} else {
		let data = {
			userId: userData.id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	}
})


module.exports = router;