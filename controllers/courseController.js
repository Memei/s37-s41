const Course = require('../models/Course')


// Create a new course
module.exports.addCourse = (reqBody) => {
	// Create a variable "newCourse" and instantiate a new "course" onject using the mongoose model.
	// Uses the information from the request body to provide all the necessary info.
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	})
	// saves the create object to our DB using .save()
	return newCourse.save().then((course, error) => {
		// Course creation failed
		if(error) {
			return false
		// Course creation successful
		} else {
			return true
		}
	})
};

// Controller Function for retrieving all courses
module.exports.getAllCourses = (data) => {
	if(data.isAdmin) {
		return Course.find({}).then(result  => {
			return result
		})
	} else {
		return false
	}
};

// Retrieves All Active Couses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
};

// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {return result})
}

module.exports.updateCourse = (reqParams, reqBody, data) => {
	if(data) {
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
		// Syntax: findByIdAndUpdate(document ID, updatesTobeApplied)
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "You are not an Admin!"
	}
};

// Archive Course

module.exports.archiveCourse = (reqParams, reqBody, data) => {
	if(data.isAdmin) {
		let updatedCourse = {
			isActive: false
		}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
	} else {
		return "You are not authorize to make changes!"
	}
};