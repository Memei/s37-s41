// Express Setup
const express = require('express');
const mongoose = require('mongoose');
// allows our backend application to be available for use in our front end application
// Allows us to control the app's Cross-Origin Resources Sharing Settings
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

const app = express();
const port = process.env.PORT || 4000;

// Middlewares:
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Main URI
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)

// Mongoose connection
mongoose.connect(`mongodb+srv://RMS:Admin123@zuitt-batch197.kblestk.mongodb.net/s37-s41?retryWrites=true&w=majority`, {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));


app.listen(port, () => console.log(`API is now online at port: ${port}`));